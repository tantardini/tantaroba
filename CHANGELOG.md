# Changelog

<!--next-version-placeholder-->

## v0.5.0 (2024-08-21)

### Feature

* **dates:** Utils to convert today or now to string ([`c828868`](https://gitlab.com/tantardini/tantaroba/-/commit/c82886820f2b9fe4f80fc2d137b97d17dbd37577))
* **profile:** Function to compute memory usage of an object ([`d1bb8e3`](https://gitlab.com/tantardini/tantaroba/-/commit/d1bb8e3ddbb4f46e0f9b00e4fc24d72ca4d6ce0a))

## v0.4.4 (2024-03-13)

### Fix

* Allowing to use python3.9 ([`7c0dc58`](https://gitlab.com/tantardini/tantaroba/-/commit/7c0dc585fd1527215af6d11605f9291736028b19))

## v0.4.3 (2023-11-29)

### Fix

* **pyproject:** Removed loguru dependency ([`29352c6`](https://gitlab.com/tantardini/tantaroba/-/commit/29352c651577afdc5e5609b86c3d79ebad8d4545))

## v0.4.2 (2023-07-11)

### Fix

* **pyproject:** Allowing compatibility with older version of numpy ([`29bb37e`](https://gitlab.com/tantardini/tantaroba/-/commit/29bb37e64591f858f11d8d0b6c11870d647d42a7))

## v0.4.1 (2023-04-28)
### Fix
* Allowing version 1.x.x and 2.x.x of pandas ([`78b2a25`](https://gitlab.com/tantardini/tantaroba/-/commit/78b2a255b2c2899bcf7d4a9ebff82e8d1190fdd5))

## v0.4.0 (2023-03-15)
### Feature
* Removed postgresql module as it was moved to another package ([`80f1751`](https://gitlab.com/tantardini/tantaroba/-/commit/80f17518c2246cd71d4f1c2cc98e1c1ef41d4b16))

## v0.3.1 (2023-03-15)
### Fix
* **pyptoject.toml:** Using binary version of psycopg package ([`2c49b3f`](https://gitlab.com/tantardini/tantaroba/-/commit/2c49b3f8c58485e90ef2ce36f6e2ba35ccf5c6df))

## v0.3.0 (2023-02-21)
### Feature
* **strings:** Helper functions to convert from and to snake and camel case ([`e12a6a7`](https://gitlab.com/tantardini/tantaroba/-/commit/e12a6a7ac1120dc1c348d3826ece5a0def4e20e7))
* **dynaminc_import:** Added function to dynamically load classes ([`03c697b`](https://gitlab.com/tantardini/tantaroba/-/commit/03c697bf9651a8ddccc9806494d711428f112a7c))

### Fix
* **postgresql:** Fixed AttributeError arising from conflicting versions of sqlalchemy and pandas ([`0e3722c`](https://gitlab.com/tantardini/tantaroba/-/commit/0e3722cfff01aee3a132a1b01dc32d196790c5cf))

## v0.2.5 (2023-02-15)
### Fix
* Removed no more used setup.py ([`82471e9`](https://gitlab.com/tantardini/tantaroba/-/commit/82471e9d1964997c3cd330fc3a1e28b6ceb2ee25))

### Documentation
* Updated readme ([`00e64dc`](https://gitlab.com/tantardini/tantaroba/-/commit/00e64dc3d50d6b89d48dbfa17901afe5d69c7867))
* Added package information ([`773080d`](https://gitlab.com/tantardini/tantaroba/-/commit/773080d086aa45741523609e7d21cf249bf9ecd9))

## v0.2.4 (2023-02-15)
### Fix
* **pyproject:** Added empty build command to avoid publishing ([`336edb3`](https://gitlab.com/tantardini/tantaroba/-/commit/336edb3806d81716d55148127323e59a1d583e3f))

## v0.2.3 (2023-02-15)
### Fix
* Publish running only on tags ([`e9d23cf`](https://gitlab.com/tantardini/tantaroba/-/commit/e9d23cf97ec5f5aa796306c618d0f590bd73f857))

## v0.2.2 (2023-02-15)
### Fix
* **ci:** Removed not needed before_script (and forcing tagging) ([`60d9f1b`](https://gitlab.com/tantardini/tantaroba/-/commit/60d9f1b20083f9022454b1c539d30222c50a018c))
* New ci for publishing ([`1a104b0`](https://gitlab.com/tantardini/tantaroba/-/commit/1a104b0ccb74eb8a1659abecc76fc23ec9dc898b))

## v0.2.1 (2023-02-15)
### Fix
* **setup:** Added specification of subdependencies ([`995cfc2`](https://gitlab.com/tantardini/tantaroba/-/commit/995cfc228b735e648d91fb8a3e47737dbfadf367))

## v0.2.0 (2023-02-15)
### Feature
* **iterator_helpers:** Added utility function to pick object from list ([`a484533`](https://gitlab.com/tantardini/tantaroba/-/commit/a484533e0768dc1b79be61fb9dcb414a5a650726))
* **yaml:** Added utility function to read yaml files ([`9afbbbc`](https://gitlab.com/tantardini/tantaroba/-/commit/9afbbbc57debe05162ba0c697ea043d9e684fa81))
* **postgresql:** Added utility class to perform read/write operations with postgres ([`6513a50`](https://gitlab.com/tantardini/tantaroba/-/commit/6513a50fe3ba458a4959cb85487ff9dcbdb663a2))
* Added simple profiling decorator ([`c23140e`](https://gitlab.com/tantardini/tantaroba/-/commit/c23140e1b35ed6901eebb1a0763826d244cc24be))

## v0.1.0 (2022-12-14)
### Feature
* **logging:** Added configuration for basic logging ([`2d0b157`](https://gitlab.com/tantardini/tantaroba/-/commit/2d0b15780fa4038c5b571acc52d349ff5b95f022))
